﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class speech : MonoBehaviour
{
    public AudioSource scream;
    public AudioClip[] audioClipArray;
    public AudioSource _as;

    void Awake()
    {
        _as = GetComponent<AudioSource>();
    }
    void Start()
    {
    
    }


    void Update()
    {

    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "enviroment")
        {
            //scream.Play();
            _as.clip = audioClipArray[Random.Range(0, audioClipArray.Length)];
            _as.PlayOneShot(_as.clip);

            scream = GetComponent<AudioSource>();
        }
    }
}