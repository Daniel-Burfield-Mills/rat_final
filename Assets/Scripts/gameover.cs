﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameover : MonoBehaviour
{
    public Object playercam;
    public string Shop;

    void OnLevelWasLoaded()
    {
        playercam = FindObjectOfType(typeof(Camera));
    }

    // Update is called once per frame
    void Update()
    {
        if (playercam == null)
        {
           SceneManager.LoadScene(Shop);
        }
    }
}
