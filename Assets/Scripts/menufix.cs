﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menufix : MonoBehaviour
{
    public GameObject camerafixer;
    public GameObject playercam;
    void OnLevelWasLoaded()
    {
        GameObject.FindGameObjectsWithTag("menufix");
    }

    // Update is called once per frame
    void Update()
    {
        if(camerafixer != null)
        {
            playercam.SetActive(false);
        }
    }
}
