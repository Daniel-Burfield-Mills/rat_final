﻿// These are some generally expected includes that Unity will pre-add for you, generally no need to touch them
using System.Collections;
using System.Collections.Generic;
// This is the most important include (as it gives us access to the UnityEngine structures)
using UnityEngine;

// CLASS 
// Jumper (MonoBehaviour) - inheriting from MonoBehaviour to ensure we can operate within Unity
// DESCRIPTION 
// Has simplistic functionality to allow the player to "jump" towards the mouse cursor
// Can be extended in a variety of ways to enable more interesting behaviours


public class Jumper : MonoBehaviour
{
    bool jump = false;
    public LayerMask ground;



    // This is an inspector-level element that creates a header in our script inspector in Unity
    // Use these to separate out big blocks of inspector values
    [Header("Jumping")]

    // The tooltip applies to the next inspectable value and provides information when it's hovered in the inspector in Unity
    [Tooltip("The speed that we travel when we jump")] 
    // This is how fast we will travel when we jump in the scene (turn it up to jump faster, down to jump slower)
    public float jumpStrength = 20.0f;

    // The Update function is called every render frame by Unity to handle object processing (can be public/private/protected/etc.)
    void Update()
    {
       
        // If we press the left mouse button (only triggered first frame of pressing)
        if (Input.GetKeyDown(KeyCode.Space) && jump == false)
        {
            // Get our Rigidbody2D and set its velocity to towards the mouse at the strength of our jump
            gameObject.GetComponent<Rigidbody2D>().AddForce (new Vector2(0, 1) * jumpStrength);
            jump = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        jump = false;
        
    }
}
