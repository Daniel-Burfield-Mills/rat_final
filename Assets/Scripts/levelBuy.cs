﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class levelBuy : MonoBehaviour
{
   public bool levelTwoBuy, levelThreeBuy;

    private void Start()
    {
        restorePurchase();
        updatePurchase();
    }

   
    private void updatePurchase()
    {
        PlayerPrefs.SetString("levelTwoBuy", levelTwoBuy.ToString());
        PlayerPrefs.SetString("levelThreeBuy", levelThreeBuy.ToString());
    }

    private void restorePurchase()
    {
        levelTwoBuy = Boolean.Parse(PlayerPrefs.GetString("levelTwoBuy", "false"));
        levelThreeBuy = Boolean.Parse(PlayerPrefs.GetString("levelThreeBuy", "false"));
    }

    public void levelTwo()
    {
        updatePurchase();

        if (levelTwoBuy == false)
        {
            if (PlayerPrefs.GetInt("Coins") < 100)
            {
                Debug.Log("You are too poor!");
            }
            else if (PlayerPrefs.GetInt("Coins") >= 100)
            {
                levelTwoBuy = true;
                PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - 100);
               
            }
        }
        else if (levelTwoBuy == true)
        {
            SceneManager.LoadScene("level2");
        }
    }

    public void levelThree()
    {
        updatePurchase();

        if (levelThreeBuy == false)
        {
            if (PlayerPrefs.GetInt("Coins") < 150)
            {
                Debug.Log("You are too poor!");
            }
            else if (PlayerPrefs.GetInt("Coins") >= 150)
            {
                levelThreeBuy = true;
                PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - 150);

            }
        }
        else if (levelThreeBuy == true)
        {
            SceneManager.LoadScene("level3");
        }
    }
}
